<?php

namespace Cesi\Msi\Archi\Command;

use Cesi\Msi\Archi\FileReader;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ReadCsv
 */
class ReadCsvCommand extends Command
{
    /**
     * Name of the command (the part after "bin/console")
     * @var string
     */
    protected static $defaultName = 'read:csv';

    protected function configure(): void
    {
        $this->setDescription("Return headers of the website in CSV");

        $this->addArgument(
            "filepath",
            InputArgument::REQUIRED,
            "The path of the url csv",
        );

        $this->addOption(
            "separator",
            "s",
            InputArgument::OPTIONAL,
            "Separator in the CSV",
            ";"
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filePath = $input->getArgument("filepath");
        $separator = $input->getOption("separator");


        // 2) Parser le fichier csv
        $file_reader = new FileReader($filePath, $separator);
        try {
            $csv_arr = $file_reader->read();
        } catch (Exception $e) {
            $output->writeln("ERROR : Invalid file path $filePath !");
            return 1;
        }

        // 3) Parcourir les lignes csv
        foreach ($csv_arr as $row) {
            // 3.1) Pour chaque ligne extraire les données
            $url = $row[0] ?? '';
            $title = $row[1] ?? '';

            // 3.2) Récupérer les headers de l'url pour cette ligne
            $headers = $file_reader->getUrlHeaders($url);
            // 3.3) Construire un tableau qui merge les lignes actuelles du csv + les headers
            $res = array_merge([$url, $title], $headers);

            // 4) Afficher les données construites en csv sur STDOUT
            // $output->writeln($res);
            // $output->writeln($separator);
            // TODO : display on the real STDOUT
            $file_reader->display($res, $separator);
            //fputcsv($output, $res, $separator);
        }
        $output->writeln("Success !");
        return 0;
    }
}