<?php

namespace Cesi\Msi\Archi;

use Exception;

class FileReader
{

    private string $csv;
    private string $separator;

    /**
     * @return string
     */
    public function getCsv(): string
    {
        return $this->csv;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * src constructor.
     * @param string $csv
     * @param string $separator
     */
    public function __construct(string $csv, string $separator)
    {
        $this->csv = $csv;
        $this->separator = $separator;
    }

    /**
     * @param string $key
     * @param array $header
     * @return string
     */
    private function getHeaderValues(string $key, array $header): string
    {

        $result = "";
        if (array_key_exists($key, $header)) {
            if (is_array($header[$key])) {
                $array_header = $header[$key];
                $array_length = count($array_header);
                for ($i = 0; $i < $array_length; $i++) {
                    if ($i + 1 < $array_length) {
                        $result = $result . $array_header[$i] . ",";
                    } else {
                        $result = $result . $array_header[$i];
                    }
                }
            } else {
                $result = $header[$key];
            }
        } else {
            $result = "No {$key}";
        }
        return $result;

    }

    /**
     * @param string $url
     * @return array
     */
    public function getUrlHeaders(string $url): array
    {
        $headers = get_headers($url, 1);
        return $this->parseHeaders($headers);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function read(): array
    {
        $csv = $this->csv;
        $separator = $this->separator;
        $line = [];
        $file = fopen($csv, 'r');
        if (!$file) {
            throw new Exception("Unable to open file " . $csv);
        }
        while (!feof($file)) {
            $line[] = fgetcsv($file, 1024, $separator);
        }
        fclose($file);
        return $line;
    }

    /**
     * @param array $csv_arr
     * @param string $separator
     */
    public function display(array $csv_arr, string $separator): void
    {
        fputcsv(STDOUT, $csv_arr, $separator);
    }

    /**
     * @param array $headers
     * @return array
     */
    public function parseHeaders(array $headers): array
    {

        $results = [
            'Content-Type' => '',
            'Expires' => '',
            'Cache-Control' => '',
        ];

        foreach ($results as $resultKey => $values) {
            $results[$resultKey] = self::getHeaderValues($resultKey, $headers);
        }
        return $results;
    }
}
