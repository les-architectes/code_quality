<?php

namespace Cesi\Msi\Archi\Tests\Command;

use Cesi\Msi\Archi\Command\ReadCsvCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class ReadCsvCommandTest extends TestCase
{
    public function testExecute()
    {
        $command = new ReadCsvCommand();
        $commandTest = new CommandTester($command);

        $filePath = "./url.csv";
        $separator = ",";

        /*$commandTest->execute([
           "-V"
       ]);*/
        $commandTest->execute([
           "filepath" => $filePath,
           "-s" => $separator
        ]);

        $display = $commandTest->getDisplay();
        $this->assertStringContainsString("Success !", $display);

//        $this->assertStringContainsString("https://www.lequipe.fr/,lequipe,", $display);
//        $this->assertStringContainsString("https://www.sofoot.com/,sofoot,text/html,", $display);
//        $this->assertStringContainsString('https://wolcen-universe.com/,"wolcen build",', $display);
    }
}
